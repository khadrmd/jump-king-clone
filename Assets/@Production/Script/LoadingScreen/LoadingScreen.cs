using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    static AsyncOperation operation;

    public static void LoadScene(string sceneName)
    {
        Debug.Log("Loading Started");
        GameObject loading = Instantiate(Resources.Load<GameObject>("LoadingScene/LoadingScreenPref"));
        DontDestroyOnLoad(loading);
        operation = SceneManager.LoadSceneAsync(sceneName);
    }
}
