using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    static GameObject pauseMenu;

    private void Start()
    {
        pauseMenu = Resources.Load<GameObject>("Canvas_Pause");
    }

    public void StartGame()
    {
        AudioManager.playAudio("click");
        Time.timeScale = 1f;
        GameSceneManager.LoadScene("GameScene");
        AudioManager.playAudio("bgm");
    }

    public void ExitGame()
    {
        AudioManager.playAudio("click");
        Application.Quit();
    }

    public static void pauseGame()
    {
        if(Time.timeScale == 1f)
        {
            Time.timeScale = 0f;
            Instantiate(pauseMenu);
        }
        else
        {
            Time.timeScale = 1f;
            if (GameObject.Find("Canvas_Pause(Clone)") != null) Destroy(GameObject.Find("Canvas_Pause(Clone)"));
            else return;
        }
    }
}
