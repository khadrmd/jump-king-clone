using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInput : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        //Change scene if player entered door
        if (Input.GetKeyDown(KeyCode.E) && GameSceneManager.Instance.nextScene != ESceneName.None)
        {
            SceneManager.LoadScene((int)GameSceneManager.Instance.nextScene);
        }
    }
}
