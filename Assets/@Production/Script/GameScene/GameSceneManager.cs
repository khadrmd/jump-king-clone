using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{

    public static GameSceneManager Instance = null;

    [HideInInspector]
    public ESceneName nextScene = ESceneName.None;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public static void LoadScene(string sceneName)
    {
        Instance.StartCoroutine(StartLoading(sceneName));
    }

    public static IEnumerator StartLoading(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        GameObject loading = Instantiate(Resources.Load<GameObject>("LoadingScene/LoadingScreenPref"));
        DontDestroyOnLoad(loading);
        while (!operation.isDone)
        {
            yield return null;
        }
        Destroy(loading);
    }


}
