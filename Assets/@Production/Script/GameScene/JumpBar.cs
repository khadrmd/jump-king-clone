using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpBar : MonoBehaviour
{
    public Slider slider;

    public void SetBarValue(int jump)
    {
        slider.value = jump;
    }

}
