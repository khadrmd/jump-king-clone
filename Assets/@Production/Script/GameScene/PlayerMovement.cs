using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    
    //Jump Bar
    public JumpBar jumpBar;
    public GameObject JumpBar;

    //Bouncing
    public PhysicsMaterial2D normalMat, bounceMat;

    //Moving speed and groundcheck
    public float speed;
    float jumpSpeed = 14f;
    public float defaultSpeed = 7f;
    private float maxSpeed = 120;
    public bool inputActive;
    public bool TouchingIce;
    public bool iceMovement;

    //Flip
    bool facingRight = true;
    bool facingLeft = false;

    //Jumping
    float timeWhenPressed, totalTimePressed;
    float maxTimePressed = 0.8f;
    float baseJumpForce = 5f;
    float maxJumpForce = 40f;
    float totalJumpForce;

    //Flag checkpoint
    Vector2 flagCPpos;
    bool checkpointReached;

    //Animation
    Animator animator;

    //Activate ending canvas
    public GameObject endCanvas;

    //Player light
    public GameObject _light;

    void Start()
    {
        speed = defaultSpeed;
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = defaultSpeed;
        jumpBar.SetBarValue(0);
        JumpBar.SetActive(false);
        animator = GetComponent<Animator>();
    }


    void Update()
    {
        //Flip
        if (Time.timeScale != 0)
        {
            if (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)))
            {
                facingLeft = true;
                facingRight = false;
            }
            else if (Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)))
            {
                facingRight = true;
                facingLeft = false;
            }
            if (facingRight)
            {
                transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
            else if (facingLeft)
            {
                transform.localScale = new Vector2(-Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
        }

        //Movement
        float move = Input.GetAxisRaw("Horizontal");

        //Raycasts
        RaycastHit2D hitDownLeft = Physics2D.Raycast(new Vector2(transform.position.x - 0.5f, transform.position.y - 0.6f), -Vector2.up, 1f);
        RaycastHit2D hitDownRight = Physics2D.Raycast(new Vector2(transform.position.x + 0.5f, transform.position.y - 0.6f), -Vector2.up, 1f);
        RaycastHit2D groundHitDownLeft = Physics2D.Raycast(new Vector2(transform.position.x - 0.45f, transform.position.y - 0.6f), -Vector2.up, 0.1f);
        RaycastHit2D groundHitDownRight = Physics2D.Raycast(new Vector2(transform.position.x + 0.45f, transform.position.y - 0.6f), -Vector2.up, 0.1f);
        //Make sure to bounce
        if ((hitDownLeft.collider != null || hitDownRight.collider != null) && rb.velocity.y <= 0)
        {
            rb.sharedMaterial = normalMat;
        }
        else
        {
            rb.sharedMaterial = bounceMat;
        }
        //Ground check
        if (groundHitDownLeft.collider != null || groundHitDownRight.collider != null)
        {
            inputActive = true;
            animator.SetBool("isReady", false);
            animator.SetBool("isJumping", false);
        }
        else
        {
            inputActive = false;
        }

        //Movement Effect & Jump
        if (inputActive)
        {
            rb.velocity = new Vector2(move * speed, rb.velocity.y);
            if (Input.GetKey(KeyCode.Space))
            {
                animator.SetBool("isReady", true);
                animator.SetBool("isJumping", false);
                rb.velocity = new Vector2(0f, 0f);

                JumpPercentage();

                JumpBar.SetActive(true);

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                timeWhenPressed = Time.time;
                speed = jumpSpeed;
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                AudioManager.playAudio("jump");
                animator.SetBool("isReady", false);
                animator.SetBool("isJumping", true);
                jump();
                jumpBar.SetBarValue(0);
                JumpBar.SetActive(false);
            }

            if (TouchingIce)
            {
                iceMovement = true;
            }
            else
            {
                iceMovement = false;
            }

            if (iceMovement && TouchingIce)
            {
                rb.AddForce(new Vector2(move * maxSpeed, 0));
            }
        }

        //Make sure to reset jump strength when falling
        if (rb.velocity.y < 0)
        {
            timeWhenPressed = Time.time;
        }

        //Flag checkpoint
        if (checkpointReached && Input.GetKeyDown(KeyCode.C))
        {
            transform.position = flagCPpos;
        }

        //Animation
        animator.SetFloat("isRunning", Mathf.Abs(rb.velocity.x));
        animator.SetBool("isGrounded", inputActive);

        //Pause Game
        if (Input.GetKeyDown(KeyCode.Escape)) UIManager.pauseGame();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "FlagCP")
        {
            flagCPpos = collision.gameObject.transform.position;
            checkpointReached = true;
        }
        if(collision.gameObject.tag == "Pink")
        {
            collision.transform.GetChild(0).gameObject.SetActive(true);
            endCanvas.SetActive(true);
        }
        if(collision.gameObject.tag == "LastRoom")
        {
            _light.SetActive(false);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            speed = defaultSpeed;
            AudioManager.playAudio("land");
            this.transform.parent = collision.transform;
        }

        if(collision.gameObject.tag == "Ice")
        {
            TouchingIce = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    { 
        if(collision.gameObject.tag == "Ice")
        {
            TouchingIce = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            speed = defaultSpeed;
            this.transform.parent = null;
        }

        if (collision.gameObject.tag == "Ice") 
        {
            TouchingIce = false;
        }
    }

    void jump()
    {
        rb.sharedMaterial = bounceMat;
        totalTimePressed = Time.time - timeWhenPressed;
        if (totalTimePressed >= maxTimePressed)
        {
            totalTimePressed = maxTimePressed;
        }
        totalJumpForce = baseJumpForce + (totalTimePressed * Mathf.Floor((maxJumpForce - baseJumpForce) / maxTimePressed));
        if (totalJumpForce >= maxJumpForce)
        {
            rb.velocity = new Vector2(rb.velocity.x, maxJumpForce);
        }
        else
        {
            rb.velocity = new Vector2(rb.velocity.x, totalJumpForce);
        }
    }

    void JumpPercentage()
    {
        totalTimePressed = Time.time - timeWhenPressed;
        float timePercentage = Mathf.Min(1,(totalTimePressed/ maxTimePressed))* 100; 
        
        jumpBar.SetBarValue((int)timePercentage);

    }
}
