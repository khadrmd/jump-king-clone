using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioClip playerJump, playerLand, gameBgm, button;
    static AudioSource sfxSrc;
    static AudioSource bgmSrc;

    static AudioManager Instance = null;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    private void Start()
    {
        Instance = this;
        sfxSrc = transform.GetChild(0).GetComponent<AudioSource>();
        bgmSrc = transform.GetChild(1).GetComponent<AudioSource>();

        playerJump = Resources.Load<AudioClip>("PlayerJump");
        playerLand = Resources.Load<AudioClip>("PlayerLand2");
        gameBgm = Resources.Load<AudioClip>("BGM_level2");
        button = Resources.Load<AudioClip>("buttonSFX");
    }

    public static void playAudio(string clip)
    {
        switch (clip)
        {
            case ("jump"):
                sfxSrc.PlayOneShot(playerJump);
                break;
            case ("land"):
                sfxSrc.PlayOneShot(playerLand);
                break;
            case ("bgm"):
                bgmSrc.clip = gameBgm;
                bgmSrc.loop = true;
                bgmSrc.Play();
                break;
            case ("click"):
                sfxSrc.PlayOneShot(button);
                break;
        }
    }
}
