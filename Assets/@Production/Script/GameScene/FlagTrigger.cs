using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FlagTrigger : MonoBehaviour
{
    SpriteRenderer SR;
    private void Start()
    {
        SR = GetComponent<SpriteRenderer>();       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            SR.color = Color.gray;
        }
    }
}
