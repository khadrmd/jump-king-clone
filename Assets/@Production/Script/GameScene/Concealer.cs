using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Concealer : MonoBehaviour
{
    public GameObject concealer;

    private void OnTriggerEnter2D(Collider2D SecretRoom)
    {
        if (SecretRoom.CompareTag("Player") && !SecretRoom.isTrigger)
        {
            concealer.SetActive(false);
        }
    }
    private void OnTriggerExit2D(Collider2D SecretRoom)
    {
        if (SecretRoom.CompareTag("Player") && !SecretRoom.isTrigger)
        {
            concealer.SetActive(true);
        }
    }
}
